import {
  FUNCTION_TOKENS,
  CONDITIONAL_TOKENS,
  tokensFromMap,
  sortTokens,
  doTokensSetMatch,
  extractTokens,
  searchTokensForValues
} from './utils/tokenization.js';

import {
  scrubPunctuation,
  isAlphabet,
  giRegexOnFields
} from './utils/text.js';

import * as config from './config/tokenization.js'

let jsontkn = {
  FUNCTION_TOKENS,
  CONDITIONAL_TOKENS,
  tokensFromMap,
  sortTokens,
  doTokensSetMatch,
  extractTokens,
  searchTokensForValues,
  scrubPunctuation,
  isAlphabet,
  giRegexOnFields,
  config
}

export default jsontkn
