/**
 * Remove punctation from text
 */
export function scrubPunctuation(text, pattern=undefined) {
  if (pattern === undefined) {
    // pattern = /\b[-.,()&$#![\]{}"']+\B|\B[-.,()&$#![\]{}"']+\b/g;
    pattern = /(-?\d+(?:[.,]\d+)*)|[-.,()&$#![\]{}"']+/g;
  }
  return text.replace(pattern, "$1");
}

/**
 * Evaluates if the string contains only alphabetic characters (a-z/A-Z)
 */
export function isAlphabet(str, andSpaceQ = true) {
  let reg = andSpaceQ ? /^[a-z ]+$/i : /^[a-z]+$/i;
  return reg.test(str);
}

/**
 * global search across all fields for raw text
 * @params {string} text - raw text
 * @params {object} json - SQL-like json
 * @params {array} ids - records in json to search
 * @params {array} fields - an array of fields which occur in any given record in which to search
 * @params {object} transform - an object of {field: function} pairs, where
 * function accepts arguments (id, record, field)
 */
export function giRegexOnFields(text, json, ids, fields, transform) {
  let reg = new RegExp(text, "gi")
  let matches = [];

  if (text === "") return ids

  ids.map( id => {
    let record = json[id];
    let fieldJoin = fields.map(field => {
        let extractor = (field in transform) ? transform[field] : identity
        return extractor(id, field, record);
      }).join("");

    let match = fieldJoin.match(reg);

    if (!(match == null || match.join("") == "")) {
      matches.push(id);
    }
  });

  return matches;
}

const identity = (id, field, record) => (record[field])
