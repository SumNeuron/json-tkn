import { FUNCTION_TOKEN_MAP, CONDITIONAL_TOKEN_MAP } from "../config/tokenization.js";
import {scrubPunctuation} from './text.js';


export const FUNCTION_TOKENS = sortTokens(tokensFromMap(FUNCTION_TOKEN_MAP));
export const CONDITIONAL_TOKENS = sortTokens(tokensFromMap(CONDITIONAL_TOKEN_MAP));

/**
 * returns the tokens from map
 * @params {object} map - an object of {token: lookup} pairs
 */
export function tokensFromMap(map) {
  return Object.keys(map);
}

/**
 * Sorts tokens in the order in which to apply them
 * @params {array} tokens - a list of tokens to try and match against.
 */
export function sortTokens(tokens) {
  // copy array with .concat() to not affect order of passed item
  let tkns = tokens.concat()
  tkns.sort(function(a, b) {
    // ASC  -> a.length - b.length
    // DESC -> b.length - a.length
    return b.length - a.length;
  });
  return tkns;
}

/**
 * given two token sets, returns whether or not they are identical
 * @param {array} tokenSet1 - a list of token parts
 * @param {array} tokenSet2 - a list of token parts
 */
export function doTokensSetMatch(tokenSet1, tokenSet2) {
  if (tokenSet1.length !== tokenSet2.length) return false;
  for (let i = tokenSet1.length; i--; ) {
    if (tokenSet1[i] !== tokenSet2[i]) return false;
  }
  return true;
}



/**
 * Search raw text for tokens
 * @params {string} text - raw text
 * @params {array} fields - a list of fields that might appear in the json
 */
export function extractTokens(text, fields, pattern=undefined) {
  // store values with defaults
  let filter = {
    logic: "and",
    function: "identity",
    field: undefined,
    conditional: "eq",
    input: undefined
  };

  // remove leading and trailing whitespace
  text = text.trim();

  // remove all punctuation
  text = scrubPunctuation(text, pattern);

  // "tokenize"
  let textTokensUntouched = text.split(" ");

  // make lowercase to standardize comparisons
  let textTokens = textTokensUntouched.map(x => x.toLowerCase());

  // ASSUMPTION: if logic is specified, it will be the first word.
  let firstWord = textTokens[0].toLowerCase();

  if (firstWord === "or") {
    filter.logic = "or";
    textTokens.splice(0, 1);
    textTokensUntouched.splice(0, 1);
  } else if (firstWord === "and") {
    filter.logic = "and";
    textTokens.splice(0, 1);
    textTokensUntouched.splice(0, 1);
  } else {
    filter.logic = "and";
  }

  // ASSUMPTION: default function is the identity function
  let [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, FUNCTION_TOKENS);
  if (tokensAreEqual) {
    filter.function = FUNCTION_TOKEN_MAP[FUNCTION_TOKENS[valueIndex]];
  }

  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, CONDITIONAL_TOKENS);
  if (tokensAreEqual) {
    filter.conditional = CONDITIONAL_TOKEN_MAP[CONDITIONAL_TOKENS[valueIndex]];
  }
  // everything after the conditional is "input", so need to keep track of index
  let conditionalIndex = tokenIndex;
  let conditionalValue = ((currentValue === undefined) ? '' : currentValue);

  // SEARCH text for which field to be applied to
  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, fields);
  if (tokensAreEqual) {
    filter.field = fields[valueIndex];
  }

  // everything after the conditional is "input"
  filter.input = textTokensUntouched
    .slice(
      conditionalIndex + conditionalValue.split(" ").length,
      textTokens.length
    )
    .join(" ");

  return filter;
}


/**
 * searchs tokens for matching values
 * @params {array} tokens - an array of tokens
 * @params {array} values - an array of values from which to search for tokens
 */
export function searchTokensForValues(tokens, values) {
  let valueIndex = 0;
  let tokenIndex = 0;
  let currentValue;
  let tokenizedValue;
  let tokensAreEqual = false;
  let consecutiveTokens;

  // for each value
  for (valueIndex = 0; valueIndex < values.length; valueIndex++) {
    // current value to match in tokens
    currentValue = values[valueIndex].toLowerCase();

    // "tokenize" current, in case of multiples word are in the value
    tokenizedValue = currentValue.split(" ");

    // look at n consecutive tokens where n is the number of tokenized values
    if (tokenizedValue.length > tokens.length) continue; // too long

    for (tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
      consecutiveTokens = tokens.slice(tokenIndex, tokenIndex + tokenizedValue.length);
      tokensAreEqual = doTokensSetMatch(tokenizedValue, consecutiveTokens);
      if (tokensAreEqual) break; // early exit
    }
    if (tokensAreEqual) break; // early exit
  }
  return [tokensAreEqual, values[valueIndex], valueIndex, tokenIndex];
}
